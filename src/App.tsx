import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'react-bootstrap';
import {Stations} from "./modules/stations/stations";

import {store} from "./store/store";
import {Provider} from "react-redux";

function App() {
    return (
        <Provider store={store}>
            <Container className="mt-3">
                <Stations/>
            </Container>
        </Provider>
    );
}

export default App;
