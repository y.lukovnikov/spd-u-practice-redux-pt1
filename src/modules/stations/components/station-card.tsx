import {Station} from "../../../typedef";
import {Button, Card, Col, Row} from "react-bootstrap";
import {BiRadio} from "react-icons/all";
import {ToggleIconButton} from "../../../components/toggle-icon-button";
import React from "react";
import {useAppDispatch, useAppSelector} from "../../../store/hooks";
import {setFavourite} from "../services/actions";

interface Props {
    station: Station
}

export const StationCard = ({station}: Props) => {
    const isFavourite = useAppSelector(state => state.stations.favourites[station.stationuuid]);
    const dispatch = useAppDispatch();

    const onClick = () => {
        dispatch(
            setFavourite({id: station.stationuuid, favourite: !isFavourite})
        );
    };

    return (
        <Card>
            <Card.Body>
                <Card.Title>{station.name}</Card.Title>
                <Row>
                    <Col lg={4}>
                        {station.favicon
                            ?
                            <img alt={station.name}
                                 src={station.favicon}
                                 loading="lazy"
                                 style={{width: '100%'}}
                            />
                            : <BiRadio size="100%"/>
                        }
                    </Col>
                    <Col lg={8}>
                        <Card.Text>
                            {station.homepage}
                        </Card.Text>
                    </Col>
                </Row>
            </Card.Body>
            <Card.Footer>
                <Button>Go to page</Button>
                <ToggleIconButton active={isFavourite} onClick={onClick}/>
            </Card.Footer>
        </Card>
    )
}